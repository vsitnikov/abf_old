@echo off
set return=%cd%
cd /d %~dp0
go vet .\...
go fmt .\...
docker run --rm -v %cd%:/app -w /app golangci/golangci-lint:latest golangci-lint run --enable-all --disable wsl --disable lll --disable gochecknoglobals --disable gochecknoinits --disable gomnd --disable interfacer
cd %return%
