@echo off
set return=%cd%
cd /d %~dp0
docker run --rm -it -v %cd%:/app -w /app/internal/net golang:1.14 go test -v 2>&1
docker run --rm -it -v %cd%:/app -w /app/internal/rate golang:1.14 go test -v 2>&1
docker run --rm -it -v %cd%:/app -w /app/command/tests golang:1.14 go test -v 2>&1
docker run --rm -it -v %cd%:/app -w /app/internal/net golang:1.14 go test -v -race 2>&1
docker run --rm -it -v %cd%:/app -w /app/internal/rate golang:1.14 go test -v -race 2>&1
docker run --rm -it -v %cd%:/app -w /app/command/tests golang:1.14 go test -v -race 2>&1
cd %return%
