package grpccon

import (
	"context"
	"errors"
	"time"

	"google.golang.org/grpc"

	api "gitlab.com/vsitnikov/abf/api/grpc"
)

// Response - answer from service, if error not occur
type Response struct {
	Status bool
	Reason string
}

// Rates - struct with current settings and state
type Rates struct {
	LoginRate        int
	LoginInterval    time.Duration
	PasswordRate     int
	PasswordInterval time.Duration
	HostRate         int
	HostInterval     time.Duration
}

// Client - client to connect to service
type Client struct {
	cancel func()
	client api.ABFClient
}

// Connect - create connection to abf service
func Connect(host string) (*Client, error) {
	clientCon, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	c := &Client{}
	c.cancel = func() {
		clientCon.Close()
	}
	c.client = api.NewABFClient(clientCon)
	return c, nil
}

// Close - disconnect from service
func (c *Client) Close() {
	c.cancel()
}

// HealthCheck - check service connect and alive
func (c *Client) HealthCheck(ctx context.Context) error {
	var req api.HealthCheckRequst
	req.Apiversion = "1"
	resp, err := c.client.HealthCheck(ctx, &req)
	if err != nil {
		return err
	}
	if resp.Status != "ok" {
		return errors.New("invalid answer from remote service")
	}
	return nil
}

// LoginCheck - check login, pass and ip for bruteforce status
func (c *Client) CheckLogin(ctx context.Context, login, password, ip string) (*Response, error) {
	var req api.LoginCheckRequest
	req.Login = login
	req.Password = password
	req.Ip = ip
	resp, err := c.client.LoginCheck(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Checked
	r.Reason = resp.Reason
	return &r, nil
}

// LoginReset - check login, pass and ip for bruteforce status
func (c *Client) ResetLogin(ctx context.Context, login, ip string) (*Response, error) {
	var req api.LoginResetRequest
	req.Login = login
	req.Ip = ip
	resp, err := c.client.LoginReset(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Reseted
	r.Reason = resp.Reason
	return &r, nil
}

// WhiteListAdd - add ip or subnet into whitelist
func (c *Client) AddWhiteList(ctx context.Context, ipmask string) (*Response, error) {
	var req api.WhiteListAddRequest
	req.Ipmask = ipmask
	resp, err := c.client.WhiteListAdd(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Added
	r.Reason = resp.Reason
	return &r, nil
}

// WhiteListDelete - delete ip or subnet from whitelist
func (c *Client) DeleteWhiteList(ctx context.Context, ipmask string) (*Response, error) {
	var req api.WhiteListDeleteRequest
	req.Ipmask = ipmask
	resp, err := c.client.WhiteListDelete(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Deleted
	r.Reason = resp.Reason
	return &r, nil
}

// BlackListAdd - add ip or subnet into blacklist
func (c *Client) AddBlackList(ctx context.Context, ipmask string) (*Response, error) {
	var req api.BlackListAddRequest
	req.Ipmask = ipmask
	resp, err := c.client.BlackListAdd(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Added
	r.Reason = resp.Reason
	return &r, nil
}

// BlackListDelete - delete ip or subnet from blacklist
func (c *Client) DeleteBlackList(ctx context.Context, ipmask string) (*Response, error) {
	var req api.BlackListDeleteRequest
	req.Ipmask = ipmask
	resp, err := c.client.BlackListDelete(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Response
	r.Status = resp.Deleted
	r.Reason = resp.Reason
	return &r, nil
}

// GetRate - get current remote server rates
func (c *Client) GetRates(ctx context.Context) (*Rates, error) {
	var req api.RateRequest
	resp, err := c.client.GetRate(ctx, &req)
	if err != nil {
		return nil, err
	}
	var r Rates
	r.LoginRate = int(resp.LoginRate)
	r.LoginInterval = time.Duration(resp.LoginInterval) * time.Millisecond
	r.PasswordRate = int(resp.PasswordRate)
	r.PasswordInterval = time.Duration(resp.PasswordInterval) * time.Millisecond
	r.HostRate = int(resp.HostRate)
	r.HostInterval = time.Duration(resp.HostInterval) * time.Millisecond
	return &r, nil
}
