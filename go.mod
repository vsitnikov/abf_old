module gitlab.com/vsitnikov/abf

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/protobuf v1.3.4
	github.com/jdeal-mediamath/clockwork v0.1.0
	github.com/spf13/cobra v0.0.6
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
	google.golang.org/grpc v1.27.1
)
