@echo off
set return=%cd%
cd /d %~dp0\..
docker build -f build/package/abf.dockerfile -t vsitnikov/abf:abf .
docker build -f build/package/tests.dockerfile -t vsitnikov/abf:abftests .
cd %return%
