.PHONY: build-srv build-cli all up check run stop test clean

build-srv:
	cd scripts && ./genproto.sh && cd ../command/abf-srv && go build -o abf-srv

build-cli:
	cd command/abf-cli && go build -o ../../abf-cli

build-tests:
	cd command/tests && go build -o tests

build_all: build-srv build-cli build-tests

up: abf-srv
	cd command/abf-srv  && ./abf-srv

check:
	go vet ./...
	go fmt ./...
	golangci-lint run --enable-all --disable wsl --disable lll --disable gochecknoglobals --disable gochecknoinits --disable gomnd --disable interfacer

unittest:
	cd internal/net && go test -v 2>&1
	cd internal/rate && go test -v 2>&1
	cd command/tests && go test -v 2>&1
	cd internal/net && go test -v -race 2>&1
	cd internal/rate && go test -v -race 2>&1
	cd command/tests && go test -v -race 2>&1

run:
	docker-compose -f deployments/docker-compose.yml up -d

stop:
	docker-compose -f deployments/docker-compose.yml down

test:
	set -e; \
	export COMPOSE_IGNORE_ORPHANS=true; \
	exit_code=0; \
	docker-compose -f deployments/docker-compose.yml up -d; \
	docker-compose -f deployments/docker-compose.tests.yml up --exit-code-from tests || exit_code=$$?; \
	docker-compose -f deployments/docker-compose.yml -f deployments/docker-compose.tests.yml down; \
	make docker-clean; \
	echo "integration tests result: $$exit_code"; \
	exit $$exit_code;

docker:
	docker build -f build/package/abf.dockerfile -t vsitnikov/abf:abf .
	docker build -f build/package/tests.dockerfile -t vsitnikov/abf:abftests .

docker-push:
	docker push vsitnikov/abf:abf
	docker push vsitnikov/abf:abftests

docker-clean:
	docker rmi -f vsitnikov/abf:abftests vsitnikov/abf:abf

clean: docker-clean
	rm -f abf-cli command/abf-srv/abf-srv command/tests/tests
