@echo off
set COMPOSE_IGNORE_ORPHANS=true
docker-compose -f deployments/docker-compose.yml up -d
docker-compose -f deployments/docker-compose.tests.yml up --exit-code-from tests
set retlvl=%ErrorLevel%
docker-compose -f deployments/docker-compose.yml -f deployments/docker-compose.tests.yml down
call docker\clean.cmd
echo "Tests result: %retlvl%"