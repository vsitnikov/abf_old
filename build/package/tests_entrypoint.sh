#!/bin/bash

if [[ -z $ABF_HOST ]]; then
    echo "error: enviroment variable ABF_HOST not declared"
    exit 1
fi

./tests $ABF_HOST || exit 1

echo "Integration tests via abf-cli"
./abf-cli connect $ABF_HOST || exit 1

./abf-cli allow "192.168.100.1"
./abf-cli unallow "192.168.100.1"
./abf-cli block "192.168.100.1"
./abf-cli unblock "192.168.100.1"

echo "Check via abf-cli check [11](login,password,host), 10-passed, 11-failed"
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep passed >/dev/null 2>&1 || exit 1
./abf-cli check "login" "password" "127.0.0.1" | grep failed >/dev/null 2>&1 || exit 1
echo "Succefully tested"

./abf-cli clear "login" "127.0.0.1" || exit 1
