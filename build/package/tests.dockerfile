FROM golang:1.14 as builder
COPY . /app
WORKDIR /app/command/tests
RUN GOOS=linux go build -o tests .
WORKDIR /app/command/abf-cli
RUN GOOS=linux go build -o abf-cli .

FROM ubuntu:18.04
WORKDIR /root
COPY build/package/tests_entrypoint.sh ./
COPY --from=builder /app/command/tests/tests ./
COPY --from=builder /app/command/abf-cli/abf-cli ./

ENTRYPOINT "./tests_entrypoint.sh"

