package main

import (
	"sync"
	"time"

	"github.com/jdeal-mediamath/clockwork"
	"gitlab.com/vsitnikov/abf/internal/net"
	"gitlab.com/vsitnikov/abf/internal/rate"
	"gitlab.com/vsitnikov/abf/internal/storage"
)

// filter - main objects to filtering login attempts
type filter struct {
	whitelist *net.SubnetsList
	blacklist *net.SubnetsList
	wmx, bmx  *sync.Mutex
	counter   *rate.Counter
	limits    rate.Config
	stor      storage.Provider
}

// filterCreate - create instance of filter
func filterCreate(config RatesAndHostConfig) (*filter, error) {
	stor, err := storage.ConnectRedis(config.RedisHost, config.RedisPassword, config.RedisDB)
	if err != nil {
		return nil, err
	}
	wlProvider, err := stor.CreateSet("abfWhitelist")
	if err != nil {
		return nil, err
	}
	blProvider, err := stor.CreateSet("abfBlacklist")
	if err != nil {
		return nil, err
	}
	var f filter
	f.stor = stor
	f.whitelist, err = net.CreateSubnetsList(wlProvider)
	if err != nil {
		return nil, err
	}
	f.blacklist, err = net.CreateSubnetsList(blProvider)
	if err != nil {
		return nil, err
	}
	f.limits.Login = config.LoginRate
	f.limits.LoginDuration = time.Minute
	f.limits.Password = config.PasswordRate
	f.limits.PasswordDuration = time.Minute
	f.limits.Host = config.IPRate
	f.limits.HostDuration = time.Minute

	f.counter, err = rate.CounterCreate(f.limits, clockwork.NewRealClock())
	if err != nil {
		return nil, err
	}
	f.wmx = &sync.Mutex{}
	f.bmx = &sync.Mutex{}
	return &f, nil
}

func (f *filter) LoginCheck(login, password, hostIP string) (bool, string, error) {
	var host net.IPAddr
	if err := host.Parse(hostIP); err != nil {
		return false, "", err
	}
	f.bmx.Lock()
	inblacklist := f.blacklist.Check(host)
	f.bmx.Unlock()
	if inblacklist {
		return false, "blocked by blacklist", nil
	}
	f.wmx.Lock()
	inwhitelist := f.whitelist.Check(host)
	f.wmx.Unlock()
	if inwhitelist {
		return true, "passed by whitelist", nil
	}
	scored, reason := f.counter.CheckAndCount(login, password, hostIP)
	return scored, reason, nil
}

func (f *filter) LoginReset(login, hostip string) bool {
	return f.counter.Reset(login, hostip)
}

func (f *filter) WhiteListAdd(subnetip string) (bool, error) {
	var snet net.Subnet
	if err := snet.Parse(subnetip); err != nil {
		return false, err
	}
	f.wmx.Lock()
	added, err := f.whitelist.Add(snet)
	f.wmx.Unlock()
	return added, err
}

func (f *filter) WhiteListDelete(subnetip string) (bool, error) {
	var snet net.Subnet
	if err := snet.Parse(subnetip); err != nil {
		return false, err
	}
	f.wmx.Lock()
	deleted, err := f.whitelist.Delete(snet)
	f.wmx.Unlock()
	return deleted, err
}

func (f *filter) AddBlackList(subnetip string) (bool, error) {
	var snet net.Subnet
	if err := snet.Parse(subnetip); err != nil {
		return false, err
	}
	f.bmx.Lock()
	added, err := f.blacklist.Add(snet)
	f.bmx.Unlock()
	return added, err
}

func (f *filter) DeleteBlackList(subnetip string) (bool, error) {
	var snet net.Subnet
	if err := snet.Parse(subnetip); err != nil {
		return false, err
	}
	f.bmx.Lock()
	deleted, err := f.blacklist.Delete(snet)
	f.bmx.Unlock()
	return deleted, err
}

func (f *filter) GetLimits() rate.Config {
	return f.limits
}

func (f *filter) Close() error {
	return f.stor.Close()
}
