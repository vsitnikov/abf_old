package main

import (
	"context"
	"net"

	api "gitlab.com/vsitnikov/abf/api/grpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// ABFGRPCImpl - структура для GRPC
type ABFGRPCImpl struct {
	server    *grpc.Server
	lastError error
	logger    *zap.Logger
	hfilter   *filter
}

// openGRPCServer - service grpc interface
func openGRPCServer(config RatesAndHostConfig, zaplog *zap.Logger) (*ABFGRPCImpl, error) {
	listen, err := net.Listen("tcp", config.Host)
	if err != nil {
		return nil, err
	}
	var g ABFGRPCImpl
	g.hfilter, err = filterCreate(config)
	if err != nil {
		return nil, err
	}
	g.server = grpc.NewServer()
	g.logger = zaplog
	api.RegisterABFServer(g.server, &g)

	go func() {
		g.lastError = g.server.Serve(listen)
	}()
	return &g, nil
}

// HealthCheck - method to check service for alive
func (ab *ABFGRPCImpl) HealthCheck(ctx context.Context, in *api.HealthCheckRequst) (*api.HealthCheckResponse, error) {
	var out api.HealthCheckResponse
	out.Status = "ok"
	return &out, nil
}

// Stop - нормальная остановка GRPC
func (ab *ABFGRPCImpl) Stop() {
	ab.server.GracefulStop()
}

// LoginCheck - проверка логина
func (ab *ABFGRPCImpl) LoginCheck(ctx context.Context, in *api.LoginCheckRequest) (*api.LoginCheckResponse, error) {
	var (
		out api.LoginCheckResponse
		err error
	)
	out.Checked, out.Reason, err = ab.hfilter.LoginCheck(in.Login, in.Password, in.Ip)
	// Skip log per call. Main func with expensive rate
	return &out, err
}

// LoginReset - remove login from internal base (reset bruteforce rate)
func (ab *ABFGRPCImpl) LoginReset(ctx context.Context, in *api.LoginResetRequest) (*api.LoginResetResponse, error) {
	var out api.LoginResetResponse
	out.Reseted = ab.hfilter.LoginReset(in.Login, in.Ip)
	if out.Reseted {
		out.Reason = "exists"
	} else {
		out.Reason = "not found"
	}
	ab.logger.Info("Reset login/ip", zap.String("login", in.Login), zap.String("host", in.Ip), zap.Bool("was exist", out.Reseted))
	return &out, nil
}

// WhiteListAdd - add ip into whitelist
func (ab *ABFGRPCImpl) WhiteListAdd(ctx context.Context, in *api.WhiteListAddRequest) (*api.WhiteListAddResponse, error) {
	var out api.WhiteListAddResponse
	var err error
	out.Added, err = ab.hfilter.WhiteListAdd(in.Ipmask)
	if err != nil {
		ab.logger.Error("Add in whitelist", zap.String("mask", in.Ipmask), zap.Error(err))
		out.Reason = err.Error()
	} else {
		ab.logger.Info("Add in whitelist", zap.String("mask", in.Ipmask), zap.Bool("already exist", !out.Added))
		if !out.Added {
			out.Reason = "already exist"
		} else {
			out.Reason = "new element"
		}
	}
	return &out, err
}

// WhiteListDelete - delete ip from whitelist
func (ab *ABFGRPCImpl) WhiteListDelete(ctx context.Context, in *api.WhiteListDeleteRequest) (*api.WhiteListDeleteResponse, error) {
	var out api.WhiteListDeleteResponse
	var err error
	out.Deleted, err = ab.hfilter.WhiteListDelete(in.Ipmask)
	if err != nil {
		ab.logger.Error("Delete from whitelist", zap.String("mask", in.Ipmask), zap.Error(err))
		out.Reason = err.Error()
	} else {
		ab.logger.Info("Delete from whitelist", zap.String("mask", in.Ipmask), zap.Bool("was exist", out.Deleted))
		if !out.Deleted {
			out.Reason = "not exist"
		} else {
			out.Reason = "deleted"
		}
	}
	return &out, err
}

// BlackListAdd - add ip into blacklist
func (ab *ABFGRPCImpl) BlackListAdd(ctx context.Context, in *api.BlackListAddRequest) (*api.BlackListAddResponse, error) {
	ab.logger.Info("Add in blacklist", zap.String("mask", in.Ipmask))
	var out api.BlackListAddResponse
	var err error
	out.Added, err = ab.hfilter.AddBlackList(in.Ipmask)
	if err != nil {
		ab.logger.Error("Add in blacklist", zap.String("mask", in.Ipmask), zap.Error(err))
		out.Reason = err.Error()
	} else {
		ab.logger.Info("Add in blacklist", zap.String("mask", in.Ipmask), zap.Bool("already exist", !out.Added))
		if !out.Added {
			out.Reason = "already exist"
		} else {
			out.Reason = "new element"
		}
	}
	return &out, err
}

// BlackListDelete - delete ip from blacklist
func (ab *ABFGRPCImpl) BlackListDelete(ctx context.Context, in *api.BlackListDeleteRequest) (*api.BlackListDeleteResponse, error) {
	ab.logger.Info("Delete from blacklist", zap.String("mask", in.Ipmask))
	var out api.BlackListDeleteResponse
	var err error
	out.Deleted, err = ab.hfilter.DeleteBlackList(in.Ipmask)
	if err != nil {
		ab.logger.Error("Delete from blacklist", zap.String("mask", in.Ipmask), zap.Error(err))
		out.Reason = err.Error()
	} else {
		ab.logger.Info("Delete from blacklist", zap.String("mask", in.Ipmask), zap.Bool("was exist", out.Deleted))
		if !out.Deleted {
			out.Reason = "not exist"
		} else {
			out.Reason = "deleted"
		}
	}
	return &out, err
}

// GetRate - get current config settings of service
func (ab *ABFGRPCImpl) GetRate(ctx context.Context, in *api.RateRequest) (*api.RateResponse, error) {
	var out api.RateResponse
	limits := ab.hfilter.GetLimits()
	out.LoginRate = int32(limits.Login)
	out.LoginInterval = limits.LoginDuration.Milliseconds()
	out.PasswordRate = int32(limits.Password)
	out.PasswordInterval = limits.PasswordDuration.Milliseconds()
	out.HostRate = int32(limits.Host)
	out.HostInterval = limits.HostDuration.Milliseconds()
	return &out, nil
}
