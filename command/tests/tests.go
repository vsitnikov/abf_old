package main

import (
	"context"
	"errors"
	"log"
	"time"

	"gitlab.com/vsitnikov/abf/internal/grpccon"
)

const timeout = time.Second * 5

var tests = []func(*grpccon.Client) error{
	testHealthCheck,
	testLoginLimit,
	testPasswordLimit,
	testHostLimit,
	testWhiteList,
	testBlackList,
	testLoginLimit,
	testPasswordLimit,
	testHostLimit,
}

type checkResult struct {
	err          error
	calls        int
	loginList    map[string]int
	passwordList map[string]int
	hostList     map[string]int
}

func check(conn *grpccon.Client, loginList, passwordList, ipList func() string) *checkResult {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	var result checkResult
	result.loginList = make(map[string]int)
	result.passwordList = make(map[string]int)
	result.hostList = make(map[string]int)
	for {
		login := loginList()
		password := passwordList()
		ip := ipList()
		if login == "" || password == "" || ip == "" {
			break
		}
		result.calls++
		resp, err := conn.CheckLogin(ctx, login, password, ip)
		if resp == nil || err != nil {
			result.err = err
			return &result
		}
		if resp.Status {
			result.loginList[login]++
			result.passwordList[password]++
			result.hostList[ip]++
		}
	}
	return &result
}

func disconnect(conn *grpccon.Client, login, host string) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	_, err := conn.ResetLogin(ctx, login, host)
	return err
}

func getRate(conn *grpccon.Client) (*grpccon.Rates, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	rates, err := conn.GetRates(ctx)
	if err != nil {
		return nil, err
	}
	return rates, nil
}

func calcWithLeaked(workTime time.Duration, count int, rateInterval time.Duration) int {
	rateInMs := float64(count) / float64(rateInterval.Milliseconds())
	leaked := float64(workTime.Milliseconds()) * rateInMs
	return count + int(leaked)
}

func testHealthCheck(conn *grpccon.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return conn.HealthCheck(ctx)
}

func testLoginLimit(conn *grpccon.Client) error {
	log.Println("testLoginLimit")

	// Отсоединяемся для блокирования теста логина и пароля (при повторном заходе)
	err := disconnect(conn, "login", "192.168.1.1")
	if err != nil {
		return err
	}
	rates, err := getRate(conn)
	if err != nil {
		return err
	}

	attempts := rates.LoginRate
	login := stringGenerator(10, attempts+10, "login")
	passwords := randomString
	ip := elementGenerator("192.168.1.1", attempts+20)

	startTime := time.Now()
	res := check(conn, login, passwords, ip)
	workTime := time.Since(startTime)

	testLoginRate := res.loginList["login"]
	calcLoginRate := calcWithLeaked(workTime, rates.LoginRate, rates.LoginInterval)
	log.Printf("limits result: calls %d, login passed/calculated: %d/%d\n", res.calls, testLoginRate, calcLoginRate)

	if calcLoginRate != testLoginRate {
		return errors.New("testLoginLimit failed")
	}
	log.Println("pass: limits as service settings")
	return res.err
}

func testPasswordLimit(conn *grpccon.Client) error {
	log.Println("testPasswordLimit")

	// Отсоединяемся для блокирования теста логина и пароля (при повторном заходе)
	err := disconnect(conn, "login", "192.168.1.1")
	if err != nil {
		return err
	}
	rates, err := getRate(conn)
	if err != nil {
		return err
	}

	//	Используем случайный пароль, исключаюший конфликты при рестарте теста (блокируемый паролем)
	randomPassword := randomString()
	attempts := rates.PasswordRate
	passwords := elementGenerator(randomPassword, attempts+20)
	ip := elementGenerator("192.168.1.1", attempts+20)

	startTime := time.Now()
	login := randomString
	res := check(conn, login, passwords, ip)
	workTime := time.Since(startTime)

	testPasswordRate := res.passwordList[randomPassword]
	calcPasswordRate := calcWithLeaked(workTime, rates.PasswordRate, rates.PasswordInterval)

	log.Printf("limits result: calls %d, passwords passed/calculated: %d/%d\n",
		res.calls, testPasswordRate, calcPasswordRate)

	if calcPasswordRate != testPasswordRate {
		return errors.New("testPasswordLimit failed")
	}

	log.Println("pass: limits as service settings")
	return res.err
}

func testHostLimit(conn *grpccon.Client) error {
	log.Println("testHostLimit")
	host := "192.168.0.1"
	err := disconnect(conn, "", host)
	if err != nil {
		return err
	}
	rates, err := getRate(conn)
	if err != nil {
		return err
	}

	attempts := rates.HostRate
	passwords := randomString
	login := randomString
	ip := ipGenerator(300, "192.168.0.0", attempts+100, host)

	startTime := time.Now()
	res := check(conn, login, passwords, ip)
	workTime := time.Since(startTime)

	calcHostRate := calcWithLeaked(workTime, rates.HostRate, rates.HostInterval)
	testHostRate := res.hostList[host]
	log.Printf("limits result: calls %d, ip passed/calculated %d/%d\n", res.calls, testHostRate, calcHostRate)

	if calcHostRate != testHostRate {
		return errors.New("testHostLimit failed")
	}
	log.Println("pass: limits as service settings")
	return res.err
}

func testWhiteList(conn *grpccon.Client) error {
	log.Println("testWhiteList")
	rates, err := getRate(conn)
	if err != nil {
		return err
	}

	host := "192.168.2.1"
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	log.Println("Add into whitelist", host)
	result, err := conn.AddWhiteList(ctx, host)
	printResult(result, err)
	if err != nil {
		return err
	}

	hostCalls := rates.HostRate + 100

	login := randomString
	passwords := randomString
	ip := ipGenerator(200, "192.168.2.0", hostCalls, host)
	res := check(conn, login, passwords, ip)

	testIPRate := res.hostList[host]
	log.Printf("limits result: calls %d, passed ip: %d\n", res.calls, testIPRate)
	if hostCalls != testIPRate {
		return errors.New("testWhiteList failed")
	}
	log.Println("pass: limits as service settings")

	log.Println("Remove from whitelist", host)
	result, err = conn.DeleteWhiteList(ctx, host)
	printResult(result, err)
	if err != nil {
		return err
	}
	return res.err
}

func testBlackList(conn *grpccon.Client) error {
	log.Println("testBlackList")
	rates, err := getRate(conn)
	if err != nil {
		return err
	}

	host := "192.168.3.1"
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	log.Println("Add into blacklist", host)
	result, err := conn.AddBlackList(ctx, host)
	printResult(result, err)
	if err != nil {
		return err
	}

	attempts := rates.HostRate + 100
	login := randomString
	passwords := randomString
	ip := ipGenerator(200, "192.168.3.0", attempts, host)
	res := check(conn, login, passwords, ip)

	testIPRate := res.hostList[host]
	log.Printf("limits result: calls %d, passed ip: %d\n", res.calls, testIPRate)
	if testIPRate != 0 {
		return errors.New("testBlackList failed")
	}
	log.Println("pass: limits as service settings")

	log.Println("Remove from blacklist", host)
	result, err = conn.DeleteBlackList(ctx, host)
	printResult(result, err)
	if err != nil {
		return err
	}

	return res.err
}
