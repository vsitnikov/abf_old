package main

import (
	"log"
	"math/rand"
	"time"

	"gitlab.com/vsitnikov/abf/internal/net"
)

func init() {
	now := time.Now()
	rand.Seed(now.UnixNano())
}

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randomString() string {
	letterLength := len(letters)
	passwordSize := rand.Intn(4) + 4
	password := make([]rune, passwordSize)
	for i := range password {
		password[i] = letters[rand.Intn(letterLength)]
	}
	return string(password)
}

func randomIP(mask string) string {
	var ip net.IPAddr

	if err := ip.Parse(mask); err != nil {
		log.Fatal(err)
	}

	base := net.Unpackip(ip)
	for i := 0; i < 4; i++ {
		if base[i] == 0 {
			base[i] = byte(100 + rand.Intn(100))
		}
	}
	randomIP := net.Packip(base)
	return randomIP.String()
}

func ipGenerator(randomCount int, mask string, count int, ip string) func() string {
	return func() string {
		if randomCount == 0 && count == 0 {
			return ""
		}
		n := rand.Intn(100)
		if n%2 == 0 {
			if count > 0 {
				count--
				return ip
			}
			randomCount--
			return randomIP(mask)
		}
		if randomCount > 0 {
			randomCount--
			return randomIP(mask)
		}
		count--
		return ip
	}
}

func elementGenerator(element string, count int) func() string {
	return func() string {
		if count > 0 {
			count--
			return element
		}
		return ""
	}
}

func stringGenerator(randomCount, count int, good string) func() string {
	return func() string {
		if randomCount == 0 && count == 0 {
			return ""
		}
		n := rand.Intn(100)
		if n%2 == 0 {
			if count > 0 {
				count--
				return good
			}
			randomCount--
			return randomString()
		}
		if randomCount > 0 {
			randomCount--
			return randomString()
		}
		count--
		return good
	}
}
