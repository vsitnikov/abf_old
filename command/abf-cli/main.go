package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var cmdConnect = &cobra.Command{
	Use:                   "connect <host>",
	Short:                 "Connect to bruteforce service",
	Long:                  "Connect to bruteforce service.\nAll next commands will work with this host.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := connectCommand(args[0])
		stopOnError(err)
	},
}

var cmdDisconnect = &cobra.Command{
	Use:                   "disconnect",
	Short:                 "Disconnect from current bruteforce service",
	Long:                  "Disconnect from current bruteforce service.\nAfter disconnecting it is need connect to service for sending new command",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		err := disconnectCommand()
		stopOnError(err)
	},
}

var cmdClear = &cobra.Command{
	Use:                   "clear <login> <ip>",
	Short:                 "Clear login + host from bruteforce lists",
	Long:                  "Clear (remove) host from any blockers in service.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		err := clearCommand(args[0], args[1])
		stopOnError(err)
	},
}

var cmdAllow = &cobra.Command{
	Use:                   "allow <host|subnet>",
	Short:                 "Add host or subnet into white list",
	Long:                  "Add host or subnet into white list.\nHosts in white list always passed without checking.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := allowCommand(args[0])
		stopOnError(err)
	},
}

var cmdUnallow = &cobra.Command{
	Use:                   "unallow <host|subnet>",
	Short:                 "Remove host or subnet from white list",
	Long:                  "Remove host or subnet from white list.\nHost will be processed with rules.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := unallowCommand(args[0])
		stopOnError(err)
	},
}

var cmdBlock = &cobra.Command{
	Use:                   "block <host|subnet>",
	Short:                 "Add host or subnet into black list",
	Long:                  "Add host or subnet into black list.\nHosts into black list always be blocked.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := blockCommand(args[0])
		stopOnError(err)
	},
}

var cmdUnblock = &cobra.Command{
	Use:                   "unblock <host|subnet>",
	Short:                 "Remove host or subnet from black list",
	Long:                  "Remove host or subnet from black list.\nHost will be processed with rules.",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		err := unblockCommand(args[0])
		stopOnError(err)
	},
}

var cmdCheck = &cobra.Command{
	Use:                   "check <login> <password> <host>",
	Short:                 "Check state in service for login/password/host",
	Long:                  "Try login with parameters and print response from service",
	DisableFlagsInUseLine: true,
	Args:                  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		err := checkCommand(args[0], args[1], args[2])
		stopOnError(err)
	},
}

func main() {
	var rootCmd = &cobra.Command{Use: os.Args[0]}
	rootCmd.AddCommand(cmdConnect, cmdDisconnect, cmdAllow, cmdUnallow, cmdBlock, cmdUnblock, cmdClear, cmdCheck)
	rootCmd.SetHelpCommand(&cobra.Command{
		Use:    "no-help",
		Hidden: true,
	})
	err := rootCmd.Execute()
	stopOnError(err)
}

// Выход по ошибке
func stopOnError(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
